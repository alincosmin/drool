$(document).ready(function() {
    if(window.location.pathname == '/') {
        initDropdown();
        updateChart();
    }
});

function updateChart(base) {
    google.load("visualization", "1", {packages: ["corechart"], callback: drawChart});
    var baseCoin = base ? base : {name: 'USD', value: 1};

    function drawChart() {
        var request = base ? '/vcoins/base/' + base.name : '/vcoins';
        var coins = ['USD'];
        $.getJSON(request, function (data) {
            var dataarray = [
                ['Coin', baseCoin.name],
                ['USD', baseCoin.value]
            ];

            $.each(data, function () {
                dataarray.push([this.name, this.value]);
                coins.push(this.name);
            });
            var data = google.visualization.arrayToDataTable(dataarray);
            var options = {
                title: 'Virtual coin values in ' + baseCoin.name,
                legend: { position: "none" },
                width: 560,
                height: 500,
                margin: 0,
                backgroundColor: { fill: "#BBBBBB" },
                hAxis: {title: 'Coin (click on bar to change base coin)', titleTextStyle: {color: 'blue'}}
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('coinChart'));
            chart.draw(data, options);
            google.visualization.events.addListener(chart, 'select', function(){
                var coinname = coins[(chart.getSelection()[0]).row];
                if (coinname == 'USD')
                    updateChart();
                else
                     $.getJSON( '/vcoin/'+coinname, function( data ) {
                        updateChart(data[0]);
                    });
            });
        });
    }
}


function initDropdown() {
    $('.coinSelector').append($("<option/>", {
        value: 0,
        text: 'USD'
    }));

    $.getJSON( '/vcoins', function( data ) {
        $.each(data, function(){
            $('.coinSelector').append($("<option/>", {
                value: this.id,
                text: this.name
            }));
        });
    });
}

function inpageConvert(firstCurrency, firstInput, secondCurrency, secondInput){
    $(secondInput).val('...');
    var fc = $(firstCurrency + " option:selected").text();
    var sc = $(secondCurrency + " option:selected").text();
    var qty = parseFloat($(firstInput).val());

    $.post("/convert", {
        firstcoin: fc,
        secondcoin: sc,
        quantity: qty
    }, function (data) {
        $(secondInput).val(data);
    });
}