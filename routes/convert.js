var express = require('express');
var router = express.Router();

router.post('/', function(req, res){
   var missingProperties = [];
   if(!req.body.hasOwnProperty('firstcoin'))
        missingProperties.push('firstcoin');
   if(!req.body.hasOwnProperty('secondcoin'))
        missingProperties.push('secondcoin');
   if(!req.body.hasOwnProperty('quantity'))
        missingProperties.push('quantity');

   if(missingProperties.length > 0)
        return res.send(400,'Post syntax incorrect. Missing: ' + missingProperties);

   var db = req.db;
   db.collection('vcoins').find({name:req.body.firstcoin}).toArray(function (err, items){
       var firstcoin;
       if(items.length != 1) {
           if (req.body.firstcoin == 'USD')
               firstcoin = {value: 1};
           else return res.send(404, 'Coin ' + req.body.firstcoin + ' not found.');
       }
       else
           firstcoin = items[0];
       db.collection('vcoins').find({name: req.body.secondcoin}).toArray(function(err2, items2){
           var secondcoin;
           if(items2.length != 1) {
               if (req.body.secondcoin == 'USD')
                   secondcoin = {value: 1};
               else return res.send(404, 'Coin ' + req.body.secondcoin + ' not found.');
           }
           else
               secondcoin = items2[0];

           var qty = req.body.quantity;
           var result = qty*firstcoin.value/secondcoin.value;
           res.json(result);
       });
   });

});

module.exports = router;