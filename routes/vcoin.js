var express = require('express');
var router = express.Router();

router.get('/:rid', function(req, res) {
    var db = req.db;
    var coinId = parseInt(req.params.rid,10);
    if(!isNaN(coinId))
        db.collection('vcoins').find({id : coinId}).toArray(function (err, items){
            if(items.length == 0)
                res.send(404);
            res.json(items);
        });
    else
        db.collection('vcoins').find({name: req.params.rid}).toArray(function (err, items){
            if(items.length == 0)
                res.send(404);
            res.json(items);
        });
});

module.exports = router;