var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var db = req.db;
    db.collection('vcoins').find().toArray(function (err, items) {
        res.json(items);
    });
});

router.get('/base/:baseCoin', function(req, res) {
    var db = req.db;
    db.collection('vcoins').find({'name':req.params.baseCoin}).toArray(function (err, items){
        if (items.length == 1)
        {
            db.collection('vcoins').find().toArray(function (err, items2) {
                var converted = items2.map(function (item) {
                    var newValue = item.value / items[0].value;
                    return {'id' : item.id, 'name' : item.name, 'value' : newValue};
                });
                res.json(converted);
            });
        }
        else
            res.send(404);
    });
});

module.exports = router;